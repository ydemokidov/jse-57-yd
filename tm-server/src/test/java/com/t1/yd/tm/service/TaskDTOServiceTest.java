package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.dto.IProjectDtoService;
import com.t1.yd.tm.api.service.dto.ITaskDtoService;
import com.t1.yd.tm.api.service.dto.IUserDtoService;
import com.t1.yd.tm.configuration.ServerConfiguration;
import com.t1.yd.tm.enumerated.Status;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.t1.yd.tm.constant.ProjectTestData.ALL_PROJECT_DTOS;
import static com.t1.yd.tm.constant.TaskTestData.*;
import static com.t1.yd.tm.constant.UserTestData.*;


@Tag("com.t1.yd.tm.marker.UnitCategory")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ServerConfiguration.class})
public class TaskDTOServiceTest {

    @Autowired
    private ITaskDtoService service;

    @Autowired
    private IProjectDtoService projectDtoService;

    @Autowired
    private IUserDtoService userDtoService;

    @BeforeEach
    public void initRepository() {
        service.clear();
        projectDtoService.clear();
        userDtoService.clear();

        userDtoService.add(ALL_USER_DTOS);
        projectDtoService.add(ALL_PROJECT_DTOS);
    }

    @Test
    public void add() {
        service.add(USER_1_PROJECT_1_TASK_DTO_1);
        Assertions.assertEquals(USER_1_PROJECT_1_TASK_DTO_1.getId(), service.findOneById(USER_1_PROJECT_1_TASK_DTO_1.getId()).getId());
    }

    @Test
    public void addWithUser() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        Assertions.assertEquals(USER_1_PROJECT_1_TASK_DTO_1.getId(), service.findOneById(USER_1_PROJECT_1_TASK_DTO_1.getId()).getId());
    }

    @Test
    public void addAll() {
        service.add(ALL_TASK_DTOS);
        Assertions.assertEquals(ALL_TASK_DTOS.size(), service.findAll().size());
        Assertions.assertEquals(USER_1_PROJECT_1_TASK_DTO_1.getId(), service.findOneById(USER_1_PROJECT_1_TASK_DTO_1.getId()).getId());
    }

    @Test
    public void removeById() {
        service.add(ALL_TASK_DTOS);
        service.removeById(USER_1_PROJECT_1_TASK_DTO_2.getId());
        Assertions.assertEquals(ALL_PROJECT_DTOS.size() - 1, service.findAll().size());
        Assertions.assertNull(service.findOneById(USER_1_PROJECT_1_TASK_DTO_2.getId()));
    }

    @Test
    public void clear() {
        service.add(ALL_TASK_DTOS);
        service.clear();
        Assertions.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_2);
        service.add(ADMIN.getId(), ADMIN_PROJECT_1_TASK_DTO_1);
        Assertions.assertEquals(USER1_PROJECT1_TASK_DTOS.size(), service.findAll(USER_DTO_1.getId()).size());
    }

    @Test
    public void findOneByIdWithUserId() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_2);
        service.add(ADMIN.getId(), ADMIN_PROJECT_1_TASK_DTO_1);

        Assertions.assertEquals(USER_1_PROJECT_1_TASK_DTO_1.getId(), service.findOneById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId()).getId());
        Assertions.assertNull(service.findOneById(ADMIN.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId()));
    }

    @Test
    public void findOneByIndexWithUserIdPositive() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        Assertions.assertEquals(USER_1_PROJECT_1_TASK_DTO_1.getId(), service.findOneByIndex(USER_DTO_1.getId(), 1).getId());
    }

    @Test
    public void findOneByIndexWithUserIdNegative() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        Assertions.assertNull(service.findOneByIndex(ADMIN.getId(), 1));
    }

    @Test
    public void existsById() {
        service.add(USER_1_PROJECT_1_TASK_DTO_1);
        Assertions.assertTrue(service.existsById(USER_1_PROJECT_1_TASK_DTO_1.getId()));
        Assertions.assertFalse(service.existsById(USER_1_PROJECT_1_TASK_DTO_2.getId()));
    }

    @Test
    public void updateById() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.updateById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId(), TASK_NAME, TASK_DESCRIPTION);
        Assertions.assertEquals(TASK_NAME, service.findTaskById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId()).getName());
        Assertions.assertEquals(TASK_DESCRIPTION, service.findTaskById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId()).getDescription());
    }

    @Test
    public void updateByIndex() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.updateByIndex(USER_DTO_1.getId(), 1, TASK_NAME, TASK_DESCRIPTION);
        Assertions.assertEquals(TASK_NAME, service.findTaskById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId()).getName());
        Assertions.assertEquals(TASK_DESCRIPTION, service.findTaskById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId()).getDescription());
    }

    @Test
    public void changeStatusById() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.changeStatusById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId(), Status.COMPLETED);
        Assertions.assertEquals(Status.COMPLETED, service.findTaskById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId()).getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.changeStatusByIndex(USER_DTO_1.getId(), 1, Status.COMPLETED);
        Assertions.assertEquals(Status.COMPLETED, service.findTaskById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId()).getStatus());
    }

    @AfterEach
    public void clearData() {
        service.clear();
    }

}
