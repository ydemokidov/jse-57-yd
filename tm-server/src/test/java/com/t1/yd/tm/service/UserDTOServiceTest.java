package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.dto.IProjectDtoService;
import com.t1.yd.tm.api.service.dto.ITaskDtoService;
import com.t1.yd.tm.api.service.dto.IUserDtoService;
import com.t1.yd.tm.configuration.ServerConfiguration;
import com.t1.yd.tm.dto.model.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.t1.yd.tm.constant.UserTestData.*;


@Tag("com.t1.yd.tm.marker.UnitCategory")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ServerConfiguration.class})
public class UserDTOServiceTest {

    @Autowired
    private IProjectDtoService projectDtoService;

    @Autowired
    private ITaskDtoService taskDtoService;

    @Autowired
    private IUserDtoService service;

    @BeforeEach
    public void initRepository() {
        taskDtoService.clear();
        projectDtoService.clear();
        service.clear();
    }

    @Test
    public void add() {
        service.add(USER_DTO_1);
        Assertions.assertEquals(USER_DTO_1.getId(), service.findOneById(USER_DTO_1.getId()).getId());
    }

    @Test
    public void addAll() {
        service.add(ALL_USER_DTOS);
        Assertions.assertEquals(ALL_USER_DTOS.size(), service.findAll().size());
        Assertions.assertEquals(USER_DTO_1.getId(), service.findOneById(USER_DTO_1.getId()).getId());
    }


    @Test
    public void clear() {
        service.add(ALL_USER_DTOS);
        service.clear();
        Assertions.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void findByLogin() {
        service.add(USER_DTO_1);
        service.add(ADMIN);
        Assertions.assertEquals(USER_DTO_1.getId(), service.findByLogin(USER1_LOGIN).getId());
    }

    @Test
    public void findByEmail() {
        service.add(USER_DTO_1);
        Assertions.assertEquals(USER_DTO_1.getId(), service.findByEmail(USER1_EMAIL).getId());
    }

    @Test
    public void existsByLogin() {
        service.add(USER_DTO_1);
        Assertions.assertTrue(service.isLoginExist(USER1_LOGIN));
        Assertions.assertFalse(service.isLoginExist(ADMIN_LOGIN));
    }

    @Test
    public void existsByEmail() {
        service.add(USER_DTO_1);
        Assertions.assertTrue(service.isEmailExist(USER1_EMAIL));
        Assertions.assertFalse(service.isEmailExist(ADMIN_EMAIL));
    }

    @Test
    public void lockByLogin() {
        service.add(USER_DTO_1);
        service.lockByLogin(USER1_LOGIN);
        @NotNull final UserDTO tmpUserDTO = service.findByLogin(USER1_LOGIN);
        Assertions.assertTrue(tmpUserDTO.getLocked());
    }

    @Test
    public void unlockByLogin() {
        service.add(USER_DTO_1);
        service.lockByLogin(USER1_LOGIN);
        service.unlockByLogin(USER1_LOGIN);
        @NotNull final UserDTO tmpUserDTO = service.findByLogin(USER1_LOGIN);
        Assertions.assertFalse(tmpUserDTO.getLocked());
    }

    @Test
    public void removeByLogin() {
        service.add(USER_DTO_1);
        service.removeByLogin(USER1_LOGIN);
        Assertions.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void removeByEmail() {
        service.add(USER_DTO_1);
        service.removeByEmail(USER1_EMAIL);
        Assertions.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void updateUser() {
        @NotNull final String lastName = "lastname";
        @NotNull final String fstName = "fstname";
        @NotNull final String midName = "midname";
        service.add(USER_DTO_1);
        service.updateUser(USER_DTO_1.getId(), fstName, lastName, midName);
        Assertions.assertEquals(lastName, service.findOneById(USER_DTO_1.getId()).getLastName());
        Assertions.assertEquals(fstName, service.findOneById(USER_DTO_1.getId()).getFirstName());
        Assertions.assertEquals(midName, service.findOneById(USER_DTO_1.getId()).getMiddleName());
    }

    @AfterEach
    public void clearData() {
        taskDtoService.clear();
        projectDtoService.clear();
        service.clear();
    }

}
