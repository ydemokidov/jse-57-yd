package com.t1.yd.tm.repository.model;

import com.t1.yd.tm.api.repository.model.IUserRepository;
import com.t1.yd.tm.model.User;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Autowired
    public UserRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @Nullable User findByLogin(@NotNull final String login) {
        @NotNull final String query = String.format("FROM %s WHERE login=:login", getEntityName());
        return entityManager.createQuery(query, getClazz()).setParameter("login", login).setHint(QueryHints.HINT_CACHEABLE, true).getSingleResult();
    }

    @Override
    public @Nullable User findByEmail(@NotNull final String email) {
        @NotNull final String query = String.format("FROM %s WHERE email=:email", getEntityName());
        return entityManager.createQuery(query, getClazz()).setParameter("email", email).setHint(QueryHints.HINT_CACHEABLE, true).getSingleResult();
    }

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<User> getClazz() {
        return User.class;
    }

    @Override
    public User removeById(@NotNull final String id) {
        @Nullable final User user = findOneById(id);
        if (user == null) return null;
        entityManager.remove(user);
        return user;
    }

    @Override
    public User removeByIndex(@NotNull final Integer index) {
        @Nullable final User user = findOneByIndex(index);
        if (user == null) return null;
        entityManager.remove(user);
        return user;
    }

}
