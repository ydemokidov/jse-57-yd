package com.t1.yd.tm.service.dto;

import com.t1.yd.tm.api.repository.dto.IDtoTaskRepository;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.dto.ITaskDtoService;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.exception.field.*;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class TaskDtoService extends AbstractUserOwnedDtoService<TaskDTO, IDtoTaskRepository> implements ITaskDtoService {

    @Autowired
    public TaskDtoService(@NotNull final ILoggerService loggerService,
                          @NotNull final ApplicationContext context) {
        super(loggerService, context);
    }

    @NotNull
    @Override
    protected IDtoTaskRepository getRepository() {
        return context.getBean(IDtoTaskRepository.class);
    }

    @NotNull
    @Override
    public TaskDTO findTaskById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final TaskDTO taskDTO = findOneById(userId, id);
        if (taskDTO == null) throw new TaskNotFoundException();
        return taskDTO;
    }

    @NotNull
    @Override
    public TaskDTO findTaskByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final TaskDTO taskDTO = findOneByIndex(userId, index);
        if (taskDTO == null) throw new TaskNotFoundException();
        return taskDTO;
    }

    @NotNull
    @Override
    public TaskDTO removeTaskById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final TaskDTO taskDTO = removeById(userId, id);
        if (taskDTO == null) throw new TaskNotFoundException();
        return taskDTO;
    }

    @NotNull
    @Override
    public TaskDTO removeTaskByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final TaskDTO taskDTO = removeByIndex(userId, index);
        if (taskDTO == null) throw new TaskNotFoundException();
        return taskDTO;
    }

    @NotNull
    @Override
    public TaskDTO create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        if (name.isEmpty()) throw new NameEmptyException();
        if (description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final TaskDTO taskDTO = new TaskDTO(userId, name, description);
        return add(taskDTO);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO updateById(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();

        @Nullable final TaskDTO taskDTO = findOneById(userId, id);
        if (taskDTO == null) throw new TaskNotFoundException();

        taskDTO.setName(name);
        taskDTO.setDescription(description);
        update(taskDTO);

        return taskDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO updateByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new NameEmptyException();

        @Nullable final TaskDTO taskDTO = findOneByIndex(userId, index);
        if (taskDTO == null) throw new TaskNotFoundException();

        taskDTO.setName(name);
        taskDTO.setDescription(description);
        update(taskDTO);

        return taskDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();

        @Nullable final TaskDTO taskDTO = findOneById(userId, id);
        if (taskDTO == null) throw new TaskNotFoundException();

        taskDTO.setStatus(status);
        update(taskDTO);

        return taskDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final TaskDTO taskDTO = findOneByIndex(userId, index);
        if (taskDTO == null) throw new TaskNotFoundException();

        taskDTO.setStatus(status);
        update(taskDTO);

        return taskDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        List<TaskDTO> taskDtos;
        @NotNull final IDtoTaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            taskDtos = repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
        return taskDtos;
    }

}