package com.t1.yd.tm.service.model;

import com.t1.yd.tm.api.repository.model.IUserRepository;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.ISaltProvider;
import com.t1.yd.tm.api.service.model.IProjectService;
import com.t1.yd.tm.api.service.model.ITaskService;
import com.t1.yd.tm.api.service.model.IUserService;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.exception.entity.UserNotFoundException;
import com.t1.yd.tm.exception.field.EmailEmptyException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.LoginEmptyException;
import com.t1.yd.tm.exception.field.PasswordEmptyException;
import com.t1.yd.tm.exception.user.IsEmailExistException;
import com.t1.yd.tm.exception.user.IsLoginExistException;
import com.t1.yd.tm.model.User;
import com.t1.yd.tm.util.HashUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Objects;

@Service
public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final ISaltProvider saltProvider;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @Autowired
    public UserService(@NotNull final ISaltProvider saltProvider,
                       @NotNull final ILoggerService loggerService,
                       @NotNull final ApplicationContext context,
                       @NotNull final IProjectService projectService,
                       @NotNull final ITaskService taskService) {
        super(loggerService, context);
        this.projectService = projectService;
        this.taskService = taskService;
        this.saltProvider = saltProvider;
    }

    @Override
    @SneakyThrows
    public @NotNull User removeByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();

        removeById(user.getId());
        return user;
    }

    @Override
    @SneakyThrows
    public @NotNull User removeByEmail(@NotNull final String email) {
        @Nullable final User user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();

        removeById(user.getId());
        return user;
    }

    @NotNull
    @SneakyThrows
    public User remove(@NotNull final User userToRemove) {
        @Nullable final User user = findOneById(userToRemove.getId());
        if (user == null) throw new UserNotFoundException();

        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskService.clear(userToRemove.getId());
            projectService.clear(userToRemove.getId());
            removeByLogin(userToRemove.getLogin());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    @SneakyThrows
    public @NotNull User setPassword(@NotNull final String id, @NotNull final String password) {
        if (id.isEmpty()) throw new IdEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();

        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
        update(user);
        return user;
    }

    @Override
    @SneakyThrows
    public @NotNull User updateUser(@NotNull final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) {
        if (id.isEmpty()) throw new IdEmptyException();

        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();

        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);

        update(user);
        return user;
    }

    @Override
    @SneakyThrows
    public @NotNull User create(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new IsLoginExistException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
        return add(user);
    }

    @Override
    @SneakyThrows
    public @NotNull User create(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new IsEmailExistException();
        if (isLoginExist(login)) throw new IsLoginExistException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
        user.setEmail(email);
        return add(user);
    }

    @Override
    @SneakyThrows
    public @NotNull User create(@NotNull final String login, @NotNull final String password, @NotNull final String email, @Nullable Role role) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) role = Role.USUAL;
        if (isLoginExist(login)) throw new IsLoginExistException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
        user.setEmail(email);
        user.setRole(role);
        return add(user);
    }

    @Override
    public @Nullable User findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            user = repository.findByLogin(login);
            return user;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @Nullable User findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        return findByEmail(email) != null;
    }

    @Override
    @SneakyThrows
    public void lockByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        update(user);
    }

    @Override
    @SneakyThrows
    public void unlockByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
    }

    @NotNull
    @Override
    protected IUserRepository getRepository() {
        return context.getBean(IUserRepository.class);
    }
}
