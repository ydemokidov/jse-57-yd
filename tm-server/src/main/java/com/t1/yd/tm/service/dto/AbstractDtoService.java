package com.t1.yd.tm.service.dto;

import com.t1.yd.tm.api.repository.dto.IDtoRepository;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.dto.IDtoService;
import com.t1.yd.tm.dto.model.AbstractEntityDTO;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.exception.entity.EntityNotFoundException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.IndexIncorrectException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractDtoService<E extends AbstractEntityDTO, R extends IDtoRepository<E>> implements IDtoService<E> {

    protected final ApplicationContext context;

    @NotNull
    protected final ILoggerService loggerService;

    public AbstractDtoService(@NotNull final ILoggerService loggerService,
                              @NotNull final ApplicationContext context) {
        this.loggerService = loggerService;
        this.context = context;
    }

    @NotNull
    protected abstract R getRepository();

    @NotNull
    @Override
    public List<E> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @NotNull
    @Override
    @SneakyThrows
    public E add(@NotNull final E entity) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(entity);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@Nullable final Comparator comparator) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (comparator == null) return findAll();
            return repository.findAll(comparator);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(@NotNull final String id) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            return repository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByIndex(@NotNull final Integer index) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (index < 0) throw new IndexIncorrectException();
            return repository.findOneByIndex(index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeById(@NotNull final String id) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            @Nullable final E result = findOneById(id);
            if (result == null) throw new EntityNotFoundException();
            entityManager.getTransaction().begin();
            repository.removeById(id);
            entityManager.getTransaction().commit();
            return result;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeByIndex(@NotNull final Integer index) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (index < 0) throw new IndexIncorrectException();
            @Nullable final E result = findOneByIndex(index);
            if (result == null) throw new EntityNotFoundException();
            entityManager.getTransaction().begin();
            repository.removeByIndex(index);
            entityManager.getTransaction().commit();
            return result;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull final String id) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            return repository.findOneById(id) != null;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll().size();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public @NotNull Collection<E> set(@NotNull Collection<E> collection) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            collection.forEach(repository::add);
            entityManager.getTransaction().commit();
            return collection;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public @NotNull Collection<E> add(@NotNull Collection<E> collection) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            collection.forEach(repository::add);
            entityManager.getTransaction().commit();
            return collection;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public E update(@Nullable E entity) {
        if (entity == null) throw new EntityNotFoundException();
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return entity;
    }

}