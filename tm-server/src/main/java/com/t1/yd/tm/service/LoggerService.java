package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.listener.EntityListener;
import com.t1.yd.tm.listener.JmsLogProducer;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.util.logging.*;

@Service
public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String CONFIG_FILE = "/logger.properties";
    @NotNull
    private static final String COMMANDS = "COMMANDS";
    @NotNull
    private static final String COMMANDS_FILE = "./commands.xml";
    @NotNull
    private static final String ERRORS = "ERRORS";
    @NotNull
    private static final String ERRORS_FILE = "./errors.xml";
    @NotNull
    private static final String MESSAGES = "MESSAGES";
    @NotNull
    private static final String MESSAGES_FILE = "./messages.xml";

    @NotNull
    private final LogManager manager = LogManager.getLogManager();

    @NotNull
    private final Logger root = Logger.getLogger("");

    @NotNull
    private final Logger commands = Logger.getLogger(COMMANDS);

    @NotNull
    private final Logger errors = Logger.getLogger(ERRORS);

    @NotNull
    private final Logger messages = Logger.getLogger(MESSAGES);

    @NotNull
    private final ConsoleHandler consoleHandler = getConsoleHandler();

    @NotNull
    private final ApplicationContext context;

    {
        init();
        registry(commands, COMMANDS_FILE, false);
        registry(errors, ERRORS_FILE, true);
        registry(messages, MESSAGES_FILE, true);
    }

    @Autowired
    public LoggerService(@NotNull ApplicationContext context) {
        this.context = context;
    }

    private void init() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    @NotNull
    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void registry(@NotNull final Logger logger, @NotNull final String fileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(@NotNull final String message) {
        if (message.isEmpty()) return;
        messages.info(message);
    }

    @Override
    public void debug(@NotNull final String message) {
        if (message.isEmpty()) return;
        messages.fine(message);
    }

    @Override
    public void command(@NotNull final String message) {
        if (message.isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void error(@NotNull final Exception e) {
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

    @Override
    public void initJmsLogger() {
        @NotNull final JmsLogProducer jmsLogProducer = new JmsLogProducer();
        @NotNull final EntityListener entityListener = new EntityListener(jmsLogProducer);
        @NotNull final EntityManagerFactory entityManagerFactory = context.getBean(EntityManager.class).getEntityManagerFactory();
        @NotNull final SessionFactoryImpl sessionFactory = entityManagerFactory.unwrap(SessionFactoryImpl.class);
        @NotNull final EventListenerRegistry eventListenerRegistry = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_INSERT).appendListener(entityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(entityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_DELETE).appendListener(entityListener);
    }

}