package com.t1.yd.tm.repository.dto;

import com.t1.yd.tm.api.repository.dto.IDtoUserRepository;
import com.t1.yd.tm.dto.model.UserDTO;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
public class UserDtoRepository extends AbstractDtoRepository<UserDTO> implements IDtoUserRepository {

    @Autowired
    public UserDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @Nullable UserDTO findByLogin(@NotNull final String login) {
        final String query = String.format("FROM %s WHERE login = :login", getEntityName());

        return entityManager.createQuery(query, getClazz())
                .setParameter("login", login)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable UserDTO findByEmail(@NotNull final String email) {
        final String query = String.format("FROM %s WHERE email = :email", getEntityName());
        return entityManager.createQuery(query, getClazz())
                .setParameter("email", email)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<UserDTO> getClazz() {
        return UserDTO.class;
    }
}
