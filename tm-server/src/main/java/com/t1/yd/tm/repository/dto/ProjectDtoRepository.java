package com.t1.yd.tm.repository.dto;

import com.t1.yd.tm.api.repository.dto.IDtoProjectRepository;
import com.t1.yd.tm.dto.model.ProjectDTO;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
public class ProjectDtoRepository extends AbstractDtoUserOwnedRepository<ProjectDTO> implements IDtoProjectRepository {

    @Autowired
    public ProjectDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<ProjectDTO> getClazz() {
        return ProjectDTO.class;
    }

}
