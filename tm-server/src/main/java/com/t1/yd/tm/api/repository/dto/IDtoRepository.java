package com.t1.yd.tm.api.repository.dto;

import com.t1.yd.tm.dto.model.AbstractEntityDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IDtoRepository<E extends AbstractEntityDTO> {

    E add(@NotNull E entity);

    void clear();

    @NotNull
    List<E> findAll();

    @NotNull
    Collection<E> set(@NotNull Collection<E> collection);

    @NotNull
    Collection<E> add(@NotNull Collection<E> collection);

    @NotNull
    List<E> findAll(Comparator comparator);

    @Nullable
    E findOneById(@NotNull String id);

    @Nullable
    E findOneByIndex(@NotNull Integer index);

    E removeById(@NotNull String id);

    E removeByIndex(@NotNull Integer index);

    boolean existsById(@NotNull String id);

    int getSize();

    E update(@NotNull E entity);

    E remove(@NotNull E entity);

    EntityManager getEntityManager();

}
