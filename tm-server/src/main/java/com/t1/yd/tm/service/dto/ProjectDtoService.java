package com.t1.yd.tm.service.dto;

import com.t1.yd.tm.api.repository.dto.IDtoProjectRepository;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.dto.IProjectDtoService;
import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.exception.field.*;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDTO, IDtoProjectRepository> implements IProjectDtoService {

    @Autowired
    public ProjectDtoService(@NotNull final ILoggerService loggerService,
                             @NotNull final ApplicationContext context) {
        super(loggerService, context);
    }

    @NotNull
    @Override
    protected IDtoProjectRepository getRepository() {
        return context.getBean(IDtoProjectRepository.class);
    }

    @NotNull
    @Override
    public ProjectDTO findProjectById(@NotNull final String id) {
        @Nullable final ProjectDTO projectDTO = findOneById(id);
        if (projectDTO == null) throw new ProjectNotFoundException();
        return projectDTO;
    }

    @NotNull
    public ProjectDTO findProjectByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final ProjectDTO projectDTO = findOneByIndex(userId, index);
        if (projectDTO == null) throw new ProjectNotFoundException();
        return projectDTO;
    }

    @NotNull
    @Override
    public ProjectDTO findProjectById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final ProjectDTO projectDTO = findOneById(userId, id);
        if (projectDTO == null) throw new ProjectNotFoundException();
        return projectDTO;
    }

    @NotNull
    @Override
    public ProjectDTO create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        if (name.isEmpty()) throw new NameEmptyException();
        if (description.isEmpty()) throw new DescriptionEmptyException();
        return add(new ProjectDTO(userId, name, description));
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO updateById(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();

        @Nullable final ProjectDTO projectDTO = findOneById(userId, id);
        if (projectDTO == null) throw new ProjectNotFoundException();

        projectDTO.setName(name);
        projectDTO.setDescription(description);
        update(projectDTO);

        return projectDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO updateByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new NameEmptyException();

        @Nullable final ProjectDTO projectDTO = findOneByIndex(userId, index);
        if (projectDTO == null) throw new ProjectNotFoundException();

        projectDTO.setName(name);
        projectDTO.setDescription(description);
        update(projectDTO);

        return projectDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();

        @Nullable final ProjectDTO projectDTO = findOneById(userId, id);
        if (projectDTO == null) throw new ProjectNotFoundException();

        projectDTO.setStatus(status);

        update(projectDTO);
        return projectDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0 || index > findAll(userId).size()) throw new IndexIncorrectException();
        @Nullable final ProjectDTO projectDTO = findOneByIndex(userId, index);
        if (projectDTO == null) throw new ProjectNotFoundException();

        projectDTO.setStatus(status);
        update(projectDTO);

        return projectDTO;
    }

    @NotNull
    @SneakyThrows
    public ProjectDTO removeProjectById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();

        @Nullable ProjectDTO projectDTO = removeById(userId, id);
        if (projectDTO == null) throw new ProjectNotFoundException();
        return projectDTO;
    }

    @NotNull
    @SneakyThrows
    public ProjectDTO removeProjectByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final ProjectDTO projectDTO = removeByIndex(userId, index);
        if (projectDTO == null) throw new ProjectNotFoundException();
        return projectDTO;
    }

}