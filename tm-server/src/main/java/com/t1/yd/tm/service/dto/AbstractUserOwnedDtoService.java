package com.t1.yd.tm.service.dto;

import com.t1.yd.tm.api.repository.dto.IDtoUserOwnedRepository;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.dto.IUserOwnedDtoService;
import com.t1.yd.tm.dto.model.AbstractUserOwnedEntityDTO;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.exception.entity.EntityNotFoundException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.IndexIncorrectException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedDtoService<E extends AbstractUserOwnedEntityDTO, R extends IDtoUserOwnedRepository<E>> extends AbstractDtoService<E, R> implements IUserOwnedDtoService<E> {

    @Autowired
    public AbstractUserOwnedDtoService(@NotNull final ILoggerService loggerService,
                                       @NotNull final ApplicationContext context) {
        super(loggerService, context);
    }

    @Override
    @SneakyThrows
    public E add(@NotNull final String userId, @NotNull final E entity) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {

            entity.setUserId(userId);
            entityManager.getTransaction().begin();
            repository.add(entity);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (comparator == null) return findAll(userId);
            return repository.findAll(userId, comparator);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    public List<E> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            return repository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (index < 0) throw new IndexIncorrectException();
            return repository.findOneByIndex(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            @Nullable final E result = findOneById(userId, id);
            if (result == null) throw new EntityNotFoundException();
            entityManager.getTransaction().begin();
            repository.removeById(userId, id);
            entityManager.getTransaction().commit();
            return result;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (index < 0) throw new IndexIncorrectException();
            @Nullable final E result = findOneByIndex(userId, index);
            if (result == null) throw new EntityNotFoundException();
            entityManager.getTransaction().begin();
            repository.removeByIndex(userId, index);
            entityManager.getTransaction().commit();
            return result;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            return repository.findOneById(userId, id) != null;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@NotNull final String userId) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId).size();
        } finally {
            entityManager.close();
        }
    }

}
