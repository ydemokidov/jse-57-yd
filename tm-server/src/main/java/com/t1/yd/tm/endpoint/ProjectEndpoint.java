package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.api.service.IAuthService;
import com.t1.yd.tm.api.service.dto.IProjectDtoService;
import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.dto.model.SessionDTO;
import com.t1.yd.tm.dto.request.project.*;
import com.t1.yd.tm.dto.response.project.*;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.enumerated.Status;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "com.t1.yd.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    private IProjectDtoService projectService;

    @Autowired
    public ProjectEndpoint(@NotNull final IAuthService authService,
                           @NotNull final IProjectDtoService projectService) {
        super(authService);
        this.projectService = projectService;
    }

    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectChangeStatusByIdRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final String status = request.getStatus();
        @NotNull final ProjectDTO projectDTO = projectService.changeStatusById(userId, id, Status.valueOf(status));
        return new ProjectChangeStatusByIdResponse(projectDTO);
    }

    @Override
    @WebMethod
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectChangeStatusByIndexRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final String status = request.getStatus();
        @NotNull final ProjectDTO projectDTO = projectService.changeStatusByIndex(userId, index, Status.valueOf(status));
        return new ProjectChangeStatusByIndexResponse(projectDTO);
    }

    @Override
    @WebMethod
    public ProjectClearResponse clearProjects(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectClearRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        projectService.clear(userId);
        return new ProjectClearResponse();
    }

    @Override
    @WebMethod
    public ProjectCompleteByIdResponse completeProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectCompleteByIdRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final ProjectDTO projectDTO = projectService.changeStatusById(userId, id, Status.COMPLETED);
        return new ProjectCompleteByIdResponse(projectDTO);
    }

    @Override
    @WebMethod
    public ProjectCompleteByIndexResponse completeProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectCompleteByIndexRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final ProjectDTO projectDTO = projectService.changeStatusByIndex(userId, index, Status.COMPLETED);
        return new ProjectCompleteByIndexResponse(projectDTO);
    }

    @Override
    @WebMethod
    public ProjectCreateResponse createProject(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectCreateRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final ProjectDTO projectDTO = projectService.create(userId, name, description);
        return new ProjectCreateResponse(projectDTO);
    }

    @Override
    @WebMethod
    public ProjectListResponse listProjects(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectListRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final Sort sort = Sort.valueOf(request.getSort());
        @NotNull final List<ProjectDTO> projectDTOS = projectService.findAll(userId, sort);
        ProjectListResponse response = new ProjectListResponse();
        response.setProjectDTOS(projectDTOS);
        return response;
    }

    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectRemoveByIdRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final ProjectDTO projectDTO = projectService.removeProjectById(userId, id);
        return new ProjectRemoveByIdResponse(projectDTO);
    }

    @Override
    @WebMethod
    public ProjectRemoveByIndexResponse removeProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectRemoveByIndexRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final ProjectDTO projectDTO = projectService.removeProjectByIndex(userId, index);
        return new ProjectRemoveByIndexResponse(projectDTO);
    }

    @Override
    @WebMethod
    public ProjectShowByIdResponse showProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectShowByIdRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final ProjectDTO projectDTO = projectService.findProjectById(userId, id);
        return new ProjectShowByIdResponse(projectDTO);
    }

    @Override
    @WebMethod
    public ProjectShowByIndexResponse showProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectShowByIndexRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final ProjectDTO projectDTO = projectService.findProjectByIndex(userId, index);
        return new ProjectShowByIndexResponse(projectDTO);
    }

    @Override
    @WebMethod
    public ProjectStartByIdResponse startProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectStartByIdRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final ProjectDTO projectDTO = projectService.changeStatusById(userId, id, Status.IN_PROGRESS);
        return new ProjectStartByIdResponse(projectDTO);
    }

    @Override
    @WebMethod
    public ProjectStartByIndexResponse startProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectStartByIndexRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final ProjectDTO projectDTO = projectService.changeStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new ProjectStartByIndexResponse(projectDTO);
    }

    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectUpdateByIdRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final ProjectDTO projectDTO = projectService.updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(projectDTO);
    }

    @Override
    @WebMethod
    public ProjectUpdateByIndexResponse updateProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectUpdateByIndexRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final ProjectDTO projectDTO = projectService.updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(projectDTO);
    }

}