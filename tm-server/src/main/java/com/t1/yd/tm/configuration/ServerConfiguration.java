package com.t1.yd.tm.configuration;

import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.dto.model.SessionDTO;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.dto.model.UserDTO;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.model.Session;
import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.model.User;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan("com.t1.yd.tm")
public class ServerConfiguration {

    @Bean
    public EntityManagerFactory getEntityManagerFactory(@Autowired @NotNull final IPropertyService propertyService) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDbDriver());
        settings.put(Environment.URL, propertyService.getDbServer());
        settings.put(Environment.USER, propertyService.getDbUsername());
        settings.put(Environment.PASS, propertyService.getDbPassword());
        settings.put(Environment.DIALECT, propertyService.getDbSqlDialect());
        settings.put(Environment.SHOW_SQL, propertyService.getDbShowSqlFlg());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getDbStrategy());

        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDbCacheUseSecondLevel());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getDbCacheUseQuery());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getDbCacheUseMinPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getDbCacheRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDbCacheProviderConfig());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getDbCacheFactoryClass());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);

        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(User.class);

        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @Scope(value = "prototype")
    public EntityManager getEntityManager(EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

}
