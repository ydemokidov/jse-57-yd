package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.ITaskEndpoint;
import com.t1.yd.tm.api.service.IAuthService;
import com.t1.yd.tm.api.service.dto.IDtoProjectTaskService;
import com.t1.yd.tm.api.service.dto.ITaskDtoService;
import com.t1.yd.tm.dto.model.SessionDTO;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.dto.request.task.*;
import com.t1.yd.tm.dto.response.task.*;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.enumerated.Status;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "com.t1.yd.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    private ITaskDtoService taskService;

    private IDtoProjectTaskService projectTaskService;

    @Autowired
    public TaskEndpoint(@NotNull final IAuthService authService,
                        @NotNull final ITaskDtoService taskService,
                        @NotNull final IDtoProjectTaskService projectTaskService) {
        super(authService);
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    @WebMethod
    public @NotNull TaskBindToProjectResponse bindTaskToProject(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskBindToProjectRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final String projectId = request.getProjectId();
        projectTaskService.bindTaskToProject(userId, taskId, projectId);
        return new TaskBindToProjectResponse();
    }

    @Override
    @WebMethod
    public @NotNull TaskUnbindFromProjectResponse unbindTaskFromProject(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskUnbindFromProjectRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final String projectId = request.getProjectId();
        projectTaskService.unbindTaskFromProject(userId, taskId, projectId);
        return new TaskUnbindFromProjectResponse();
    }

    @Override
    @WebMethod
    public @NotNull TaskChangeStatusByIdResponse changeTaskStatusById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskChangeStatusByIdRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final String status = request.getStatus();
        @NotNull final TaskDTO taskDTO = taskService.changeStatusById(userId, taskId, Status.valueOf(status));
        return new TaskChangeStatusByIdResponse(taskDTO);
    }

    @Override
    @WebMethod
    public @NotNull TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskChangeStatusByIndexRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final String status = request.getStatus();
        @NotNull final TaskDTO taskDTO = taskService.changeStatusByIndex(userId, index, Status.valueOf(status));
        return new TaskChangeStatusByIndexResponse(taskDTO);
    }

    @Override
    @WebMethod
    public @NotNull TaskClearResponse clearTasks(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskClearRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        taskService.clear(userId);
        return new TaskClearResponse();
    }

    @Override
    @WebMethod
    public @NotNull TaskCompleteByIdResponse completeTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskCompleteByIdRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final TaskDTO taskDTO = taskService.changeStatusById(userId, taskId, Status.COMPLETED);
        return new TaskCompleteByIdResponse(taskDTO);
    }

    @Override
    @WebMethod
    public @NotNull TaskCompleteByIndexResponse completeTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskCompleteByIndexRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final TaskDTO taskDTO = taskService.changeStatusByIndex(userId, index, Status.COMPLETED);
        return new TaskCompleteByIndexResponse(taskDTO);
    }

    @Override
    @WebMethod
    public @NotNull TaskCreateResponse createTask(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskCreateRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final TaskDTO taskDTO = taskService.create(userId, name, description);
        return new TaskCreateResponse(taskDTO);
    }

    @Override
    @WebMethod
    public @NotNull TaskListByProjectIdResponse listTasksByProjectId(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskListByProjectIdRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final String projectId = request.getProjectId();
        @NotNull final List<TaskDTO> taskDTOS = taskService.findAllByProjectId(userId, projectId);
        return new TaskListByProjectIdResponse(taskDTOS);
    }

    @Override
    @WebMethod
    public @NotNull TaskListResponse listTasks(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskListRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final String sort = request.getSort();
        @NotNull final List<TaskDTO> taskDTOS = taskService.findAll(userId, Sort.valueOf(sort));
        return new TaskListResponse(taskDTOS);
    }

    @Override
    @WebMethod
    public @NotNull TaskRemoveByIdResponse removeTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskRemoveByIdRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final TaskDTO taskDTO = taskService.removeTaskById(userId, taskId);
        return new TaskRemoveByIdResponse(taskDTO);
    }

    @Override
    @WebMethod
    public @NotNull TaskRemoveByIndexResponse removeTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskRemoveByIndexRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final TaskDTO taskDTO = taskService.removeTaskByIndex(userId, index);
        return new TaskRemoveByIndexResponse(taskDTO);
    }

    @Override
    @WebMethod
    public @NotNull TaskShowByIdResponse showTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskShowByIdRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final String taskId = request.getId();
        @Nullable final TaskDTO taskDTO = taskService.findTaskById(userId, taskId);
        return new TaskShowByIdResponse(taskDTO);
    }

    @Override
    @WebMethod
    public @NotNull TaskShowByIndexResponse showTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskShowByIndexRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final Integer index = request.getIndex();
        @Nullable final TaskDTO taskDTO = taskService.findTaskByIndex(userId, index);
        return new TaskShowByIndexResponse(taskDTO);
    }

    @Override
    @WebMethod
    public @NotNull TaskStartByIdResponse startTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskStartByIdRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final TaskDTO taskDTO = taskService.changeStatusById(userId, taskId, Status.IN_PROGRESS);
        return new TaskStartByIdResponse(taskDTO);
    }

    @Override
    @WebMethod
    public @NotNull TaskStartByIndexResponse startTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskStartByIndexRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final TaskDTO taskDTO = taskService.changeStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new TaskStartByIndexResponse(taskDTO);
    }

    @Override
    @WebMethod
    public @NotNull TaskUpdateByIdResponse updateTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskUpdateByIdRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final TaskDTO taskDTO = taskService.updateById(userId, taskId, name, description);
        return new TaskUpdateByIdResponse(taskDTO);
    }

    @Override
    @WebMethod
    public @NotNull TaskUpdateByIndexResponse updateTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskUpdateByIndexRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final TaskDTO taskDTO = taskService.updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(taskDTO);
    }

}