package com.t1.yd.tm.repository.model;

import com.t1.yd.tm.api.repository.model.IRepository;
import com.t1.yd.tm.comparator.CreatedComparator;
import com.t1.yd.tm.comparator.NameComparator;
import com.t1.yd.tm.comparator.StatusComparator;
import com.t1.yd.tm.model.AbstractEntity;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    @Getter
    protected final EntityManager entityManager;

    public AbstractRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    protected abstract String getEntityName();

    @NotNull
    protected abstract Class<E> getClazz();

    @Override
    public E add(@NotNull final E entity) {
        entityManager.persist(entity);
        return entity;
    }

    @Override
    public void clear() {
        @NotNull final String query = String.format("DELETE FROM %s", getEntityName());
        entityManager.createQuery(query)
                .executeUpdate();

    }

    @Override
    public @NotNull List<E> findAll() {
        return findAll(null);
    }

    @Override
    public @NotNull Collection<E> set(@NotNull final Collection<E> collection) {
        clear();
        collection.forEach(this::add);
        return collection;
    }

    @Override
    public @NotNull Collection<E> add(@NotNull final Collection<E> collection) {
        collection.forEach(this::add);
        return collection;
    }

    @Override
    public @NotNull List<E> findAll(@Nullable final Comparator comparator) {
        @NotNull final String query = String.format("FROM %s ORDER BY :sort", getEntityName());
        return entityManager.createQuery(query, getClazz())
                .setParameter("sort", getOrderByField(comparator))
                .getResultList();

    }

    @Nullable
    @Override
    public E findOneById(@NotNull final String id) {
        @NotNull final String query = String.format("FROM %s WHERE id = :id", getEntityName());
        return entityManager.createQuery(query, getClazz())
                .setParameter("id", id)
                .getResultList()
                .stream().findFirst().orElse(null);

    }

    @Nullable
    @Override
    public E findOneByIndex(@NotNull final Integer index) {
        @NotNull final String query = String.format("FROM %s ORDER BY :sort", getEntityName());
        return entityManager.createQuery(query, getClazz())
                .setParameter("sort", getOrderByField(null))
                .setFirstResult(index - 1)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public E removeById(@NotNull final String id) {
        E entity = findOneById(id);
        if (entity == null) return null;
        return remove(entity);
    }

    @Override
    public E removeByIndex(@NotNull final Integer index) {
        E entity = findOneByIndex(index);
        if (entity == null) return null;
        return remove(entity);
    }

    @Override
    public E remove(@NotNull final E entity) {
        entityManager.remove(entity);
        return entity;
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    public int getSize() {
        @NotNull final String query = String.format("SELECT COUNT(1) FROM %s", getEntityName());
        @NotNull final Long count = entityManager.createQuery(query, Long.class).getSingleResult();
        return Math.toIntExact(count);
    }

    @Override
    public E update(@NotNull final E entity) {
        return entityManager.merge(entity);
    }

    @NotNull
    protected final String getOrderByField(@Nullable final Comparator comparator) {
        if (CreatedComparator.INSTANCE.equals(comparator)) return "created";
        if (StatusComparator.INSTANCE.equals(comparator)) return "status";
        if (NameComparator.INSTANCE.equals(comparator)) return "name";
        else return "id";
    }
}
