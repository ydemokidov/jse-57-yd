package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.dto.response.AbstractResultResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskListByProjectIdResponse extends AbstractResultResponse {

    private List<TaskDTO> taskDTOS;

    public TaskListByProjectIdResponse(@NotNull final List<TaskDTO> taskDTOS) {
        this.taskDTOS = taskDTOS;
    }

    public TaskListByProjectIdResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
