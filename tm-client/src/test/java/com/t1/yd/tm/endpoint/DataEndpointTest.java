package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IDataEndpoint;
import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.dto.request.data.*;
import com.t1.yd.tm.dto.request.project.ProjectCreateRequest;
import com.t1.yd.tm.dto.request.project.ProjectRemoveByIdRequest;
import com.t1.yd.tm.dto.request.project.ProjectShowByIdRequest;
import com.t1.yd.tm.marker.IntegrationCategory;
import com.t1.yd.tm.service.PropertyService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.t1.yd.tm.constant.EndpointTestData.ADMIN_LOGIN;
import static com.t1.yd.tm.constant.EndpointTestData.ADMIN_PASSWORD;

@Category(IntegrationCategory.class)
public class DataEndpointTest extends AbstractEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final IDataEndpoint dataEndpoint = IDataEndpoint.newInstance(propertyService);

    @Nullable
    private String token;

    @NotNull
    private ProjectDTO createProject() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest();
        request.setToken(token);
        request.setName("PROJECT_NAME");
        request.setDescription("Description");
        @Nullable final ProjectDTO projectDTOCreated = projectEndpoint.createProject(request).getProjectDTO();
        Assert.assertNotNull(projectDTOCreated);
        return projectDTOCreated;
    }

    @NotNull
    private ProjectDTO findProject(final ProjectDTO projectDTOCreated) {
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest();
        request.setToken(token);
        request.setId(projectDTOCreated.getId());
        @Nullable final ProjectDTO projectDTOFounded = projectEndpoint.showProjectById(request).getProjectDTO();
        Assert.assertNotNull(projectDTOFounded);
        return projectDTOFounded;
    }

    @Before
    public void before() {
        token = login(ADMIN_LOGIN, ADMIN_PASSWORD);
    }

    @After
    public void after() {
        loadBackup(token);
        logout(token);
    }

    @Test
    public void backupSaveLoad() {
        @NotNull final ProjectDTO projectDTO = createProject();
        @NotNull final DataBackupSaveRequest saveBackupRequest = new DataBackupSaveRequest();
        saveBackupRequest.setToken(token);
        dataEndpoint.backupSave(saveBackupRequest);
        loadBackup(token);

        @NotNull final ProjectDTO projectDTOFound = findProject(projectDTO);
        Assert.assertEquals(projectDTO.getId(), projectDTOFound.getId());
        @NotNull final ProjectRemoveByIdRequest removeByIdRequest = new ProjectRemoveByIdRequest();
        removeByIdRequest.setToken(token);
        removeByIdRequest.setId(projectDTO.getId());
        projectEndpoint.removeProjectById(removeByIdRequest);

        dataEndpoint.backupSave(saveBackupRequest);
    }

    @Test
    public void base64SaveLoad() {
        @NotNull final ProjectDTO projectDTO = createProject();
        @NotNull final DataBase64SaveRequest saveRequest = new DataBase64SaveRequest();
        saveRequest.setToken(token);
        dataEndpoint.base64Save(saveRequest);
        @NotNull final DataBase64LoadRequest loadRequest = new DataBase64LoadRequest();
        loadRequest.setToken(token);
        dataEndpoint.base64Load(loadRequest);
        @NotNull final ProjectDTO projectDTOFound = findProject(projectDTO);
        Assert.assertEquals(projectDTO.getId(), projectDTOFound.getId());
    }

    @Test
    public void binarySaveLoad() {
        @NotNull final ProjectDTO projectDTO = createProject();
        @NotNull final DataBinarySaveRequest saveRequest = new DataBinarySaveRequest();
        saveRequest.setToken(token);
        dataEndpoint.binarySave(saveRequest);
        @NotNull final DataBinaryLoadRequest loadRequest = new DataBinaryLoadRequest();
        loadRequest.setToken(token);
        dataEndpoint.binaryLoad(loadRequest);
        @NotNull final ProjectDTO projectDTOFound = findProject(projectDTO);
        Assert.assertEquals(projectDTO.getId(), projectDTOFound.getId());
    }

    @Test
    public void fasterXmlJsonSaveLoad() {
        @NotNull final ProjectDTO projectDTO = createProject();
        @NotNull final DataFasterXmlJsonSaveRequest saveRequest = new DataFasterXmlJsonSaveRequest();
        saveRequest.setToken(token);
        dataEndpoint.fasterXmlJsonSave(saveRequest);
        @NotNull final DataFasterXmlJsonLoadRequest loadRequest = new DataFasterXmlJsonLoadRequest();
        loadRequest.setToken(token);
        dataEndpoint.fasterXmlJsonLoad(loadRequest);
        @NotNull final ProjectDTO projectDTOFound = findProject(projectDTO);
        Assert.assertEquals(projectDTO.getId(), projectDTOFound.getId());
    }

    @Test
    public void jaxbJsonSaveLoad() {
        @NotNull final ProjectDTO projectDTO = createProject();
        @NotNull final DataJaxbJsonSaveRequest saveRequest = new DataJaxbJsonSaveRequest();
        saveRequest.setToken(token);
        dataEndpoint.jaxbJsonSave(saveRequest);
        @NotNull final DataJaxbJsonLoadRequest loadRequest = new DataJaxbJsonLoadRequest();
        loadRequest.setToken(token);
        dataEndpoint.jaxbJsonLoad(loadRequest);
        @NotNull final ProjectDTO projectDTOFound = findProject(projectDTO);
        Assert.assertEquals(projectDTO.getId(), projectDTOFound.getId());
    }

    @Test
    public void fasterXmlXmlSaveLoad() {
        @NotNull final ProjectDTO projectDTO = createProject();
        @NotNull final DataFasterXmlXmlSaveRequest saveRequest = new DataFasterXmlXmlSaveRequest();
        saveRequest.setToken(token);
        dataEndpoint.fasterXmlXmlSave(saveRequest);
        @NotNull final DataFasterXmlXmlLoadRequest loadRequest = new DataFasterXmlXmlLoadRequest();
        loadRequest.setToken(token);
        dataEndpoint.fasterXmlXmlLoad(loadRequest);
        @NotNull final ProjectDTO projectDTOFounded = findProject(projectDTO);
        Assert.assertEquals(projectDTO.getId(), projectDTOFounded.getId());
    }

    @Test
    public void jaxbXmlSaveLoad() {
        @NotNull final ProjectDTO projectDTO = createProject();
        @NotNull final DataJaxbXmlSaveRequest saveRequest = new DataJaxbXmlSaveRequest();
        saveRequest.setToken(token);
        dataEndpoint.jaxbXmlSave(saveRequest);
        @NotNull final DataJaxbXmlLoadRequest loadRequest = new DataJaxbXmlLoadRequest();
        loadRequest.setToken(token);
        dataEndpoint.jaxbXmlLoad(loadRequest);
        @NotNull final ProjectDTO projectDTOFound = findProject(projectDTO);
        Assert.assertEquals(projectDTO.getId(), projectDTOFound.getId());
    }

    @Test
    public void fasterXmlYmlSaveLoad() {
        @NotNull final ProjectDTO projectDTO = createProject();
        @NotNull final DataFasterXmlYmlSaveRequest saveRequest = new DataFasterXmlYmlSaveRequest();
        saveRequest.setToken(token);
        dataEndpoint.fasterXmlYmlSave(saveRequest);
        @NotNull final DataFasterXmlYmlLoadRequest loadRequest = new DataFasterXmlYmlLoadRequest();
        loadRequest.setToken(token);
        dataEndpoint.fasterXmlYmlLoad(loadRequest);
        @NotNull final ProjectDTO projectDTOFound = findProject(projectDTO);
        Assert.assertEquals(projectDTO.getId(), projectDTOFound.getId());
    }

}

