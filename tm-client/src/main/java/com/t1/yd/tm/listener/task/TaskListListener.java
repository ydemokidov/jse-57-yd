package com.t1.yd.tm.listener.task;

import com.t1.yd.tm.api.endpoint.ITaskEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.dto.request.task.TaskListRequest;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.event.ConsoleEvent;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public final class TaskListListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task_list";
    @NotNull
    public static final String DESCRIPTION = "Show list of tasks";

    @Autowired
    public TaskListListener(@NotNull final ITokenService tokenService,
                            @NotNull final ITaskEndpoint taskEndpointClient) {
        super(tokenService, taskEndpointClient);
    }

    @Override
    @EventListener(condition = "@taskListListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));

        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest();
        request.setSort(sort.toString());
        request.setToken(getToken());
        @NotNull final List<TaskDTO> taskDTOS = getTaskEndpointClient().listTasks(request).getTaskDTOS();

        int index = 1;
        for (@NotNull final TaskDTO taskDTO : taskDTOS) {
            System.out.println(index + ". " + taskDTO);
            index++;
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
