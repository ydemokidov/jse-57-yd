package com.t1.yd.tm.listener.data;

import com.t1.yd.tm.api.endpoint.IDataEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.data.DataFasterXmlYmlLoadRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.event.ConsoleEvent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class DataFasterXmlYmlLoadListener extends AbstractDataListener {

    @NotNull
    private final String name = "load_fasterxml_yml";
    @NotNull
    private final String description = "Load YAML with FasterXml library";

    @Autowired
    public DataFasterXmlYmlLoadListener(@NotNull final ITokenService tokenService,
                                        @NotNull final IDataEndpoint dataEndpointClient) {
        super(tokenService, dataEndpointClient);
    }

    @Override
    @EventListener(condition = "@dataFasterXmlYmlLoadListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA LOAD FASTERXML YML]");
        @NotNull final DataFasterXmlYmlLoadRequest request = new DataFasterXmlYmlLoadRequest();
        getDataEndpointClient().fasterXmlYmlLoad(request);
        System.out.println("[DATA LOADED]");
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}
