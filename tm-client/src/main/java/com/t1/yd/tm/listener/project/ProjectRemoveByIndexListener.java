package com.t1.yd.tm.listener.project;

import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.project.ProjectRemoveByIndexRequest;
import com.t1.yd.tm.event.ConsoleEvent;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class ProjectRemoveByIndexListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project_remove_by_index";
    @NotNull
    public static final String DESCRIPTION = "Remove project by Index";

    @Autowired
    public ProjectRemoveByIndexListener(@NotNull final ITokenService tokenService,
                                        @NotNull final IProjectEndpoint projectEndpointClient) {
        super(tokenService, projectEndpointClient);
    }

    @Override
    @EventListener(condition = "@projectRemoveByIndexListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest();
        request.setIndex(index);
        request.setToken(getToken());
        getProjectEndpointClient().removeProjectByIndex(request);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
