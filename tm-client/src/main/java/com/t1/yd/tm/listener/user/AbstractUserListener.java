package com.t1.yd.tm.listener.user;

import com.t1.yd.tm.api.endpoint.IAuthEndpoint;
import com.t1.yd.tm.api.endpoint.IUserEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.model.UserDTO;
import com.t1.yd.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractUserListener extends AbstractListener {

    private final IUserEndpoint userEndpointClient;

    private final IAuthEndpoint authEndpointClient;

    public AbstractUserListener(@NotNull final ITokenService tokenService,
                                @NotNull final IUserEndpoint userEndpointClient,
                                @NotNull final IAuthEndpoint authEndpointClient) {
        super(tokenService);
        this.userEndpointClient = userEndpointClient;
        this.authEndpointClient = authEndpointClient;
    }

    @NotNull
    protected IUserEndpoint getUserEndpoint() {
        return userEndpointClient;
    }

    @NotNull
    protected IAuthEndpoint getAuthEndpoint() {
        return authEndpointClient;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    public void showUser(@NotNull final UserDTO userDTO) {
        System.out.println("Id: " + userDTO.getId());
        System.out.println("Login: " + userDTO.getLogin());
        System.out.println("Email: " + userDTO.getEmail());
    }

}
