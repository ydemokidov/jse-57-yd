package com.t1.yd.tm.listener.task;

import com.t1.yd.tm.api.endpoint.ITaskEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.dto.request.task.TaskShowByIdRequest;
import com.t1.yd.tm.event.ConsoleEvent;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class TaskShowByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task_show_by_id";
    @NotNull
    public static final String DESCRIPTION = "Show task by Id";

    @Autowired
    public TaskShowByIdListener(@NotNull final ITokenService tokenService,
                                @NotNull final ITaskEndpoint taskEndpointClient) {
        super(tokenService, taskEndpointClient);
    }

    @Override
    @EventListener(condition = "@taskShowByIdListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");


        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest();
        request.setId(id);
        request.setToken(getToken());

        @NotNull final TaskDTO taskDTO = getTaskEndpointClient().showTaskById(request).getTaskDTO();

        showTask(taskDTO);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
