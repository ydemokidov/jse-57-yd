package com.t1.yd.tm.listener.project;

import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.project.ProjectUpdateByIndexRequest;
import com.t1.yd.tm.event.ConsoleEvent;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class ProjectUpdateByIndexListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project_update_by_index";
    @NotNull
    public static final String DESCRIPTION = "Update project by Index";

    @Autowired
    public ProjectUpdateByIndexListener(@NotNull final ITokenService tokenService,
                                        @NotNull final IProjectEndpoint projectEndpointClient) {
        super(tokenService, projectEndpointClient);
    }

    @Override
    @EventListener(condition = "@projectUpdateByIndexListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[UPDATE PROJECT BY INDEX]");

        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();

        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String desc = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest();
        request.setIndex(index);
        request.setName(name);
        request.setDescription(desc);
        request.setToken(getToken());
        getProjectEndpointClient().updateProjectByIndex(request);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
